# Static site deployment

Uses the [aws-cdk](https://github.com/aws/aws-cdk) to deploy a [static site stack](https://gitlab.com/StraightOuttaCrompton/aws-cdk-static-site).
