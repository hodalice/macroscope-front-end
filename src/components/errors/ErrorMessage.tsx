import React from "react";

import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import GenericSnackBar from "components/snackBars/GenericSnackBar";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        errorMessage: {
            color: theme.palette.error.main
        }
    })
);

interface IProps {
    message?: string;
    snackbar?: boolean;
}

export default function ErrorMessage(props: IProps) {
    const classes = useStyles();

    const { message = "An error occured", snackbar = false } = props;

    return snackbar ? (
        <GenericSnackBar
            variant="error"
            defaultIsOpen={snackbar}
            message={<Typography>{message}</Typography>}
        />
    ) : (
        <Typography className={classes.errorMessage}>{message}</Typography>
    );
}
