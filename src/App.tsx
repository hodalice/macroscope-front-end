import React from "react";

import { Switch, Route, Redirect } from "react-router-dom";

import "./App.css";
import PageLayout, { IPageLayoutTabItem } from "components/layout/PageLayout";

import WordAnalysisPage from "pages/wordAnalysis/WordAnalysisPage";
import ManualPage from "pages/manual/ManualPage";
import ContactUsPage from "pages/contactUs/ContactUsPage";
import BrowserSupportSnackBar from "components/snackBars/BrowserSupportSnackBar";

const App: React.FC = () => {
    const mainNavTabs: IPageLayoutTabItem[] = [
        { route: "/wordanalysis", label: "Word analysis", content: <WordAnalysisPage /> },
        { route: "/manual", label: "Manual", content: <ManualPage /> },
        { route: "/contactus", label: "Contact us", content: <ContactUsPage /> }
    ];

    const defaultTab = mainNavTabs[0];

    return (
        <>
            <Switch>
                <Route path="/" exact render={() => <Redirect to={defaultTab.route} />} />
                <Route
                    render={() => <PageLayout deafultTab={defaultTab} tabItems={mainNavTabs} />}
                />
            </Switch>
            <BrowserSupportSnackBar />
        </>
    );
};

export default App;
